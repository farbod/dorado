
/**
 * ==========
 *  Config
 * ==========
 * 
 */
export const Config = {
    /**
     * ===============
     *  Server Host
     * ===============
     * 
     */
    SERVER_HOST : process.env.NODE_ENV === 'production' ? 'api.somthing.somethingelse' : '127.0.0.1',

    /**
     * ===============
     *  Server Port
     * ===============
     * 
     */
    SERVER_PORT : process.env.NODE_ENV === 'production' ? '80' : '8000',

};
