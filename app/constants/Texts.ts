
/**
 * =======
 *  Texts
 * =======
 * 
 */
export const Texts = {
  /**
   * ====================
   *  Main Actions Title
   * ====================
   * 
   */
  MAIN_ACTIONS_TITLE : 'خدمات اسنپ برای سفر',

  /**
   * =======================
   *  Internal Flight Title
   * =======================
   * 
   */
  INTERNAL_FLIGHT_TITLE : 'خرید بلیط پرواز داخلی'

};
