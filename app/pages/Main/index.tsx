import { useState } from 'react';
import type { NextPage } from 'next';

import {
  Texts
} from '../../constants';
import {
  Action
} from './components';
import {
  decrement,
  increment,
  incrementByAmount,
  State
} from './slice';
import { RootState } from '../../store';
import { useAppSelector, useAppDispatch } from '../../hooks';

import styles from './index.module.scss';

const Main: NextPage = (props) => {
  const dispatch = useAppDispatch()
  const selectState = (state: RootState): State => {
    return {
      value  : state.main.value,
      status : state.main.status
    }
  };
  const state: State = useAppSelector(selectState);

  const actions = [
    { title: "پرواز داخلی", image: '/images/services/flight-internal.svg', route: '/flight-internal', enabled: true  },
    { title: "هتل داخلی",   image: '/images/services/hotel-internal.svg',  route: '/hotel-internal' , enabled: false },
    { title: "اتوبوس",      image: '/images/services/bus.svg',             route: '/bus'            , enabled: false },
    { title: "قطار",        image: '/images/services/train.svg',           route: '/train'          , enabled: false },
    { title: "پرواز خارجی", image: '/images/services/flight-external.svg', route: '/flight-external', enabled: false },
    { title: "هتل خارجی",   image: '/images/services/hotel-external.svg',  route: '/hotel-external' , enabled: false }
  ];

  return (
    <div className={styles.container}>
      <div className={styles.background}>        
        {/* <Image
          src={flightImage}
          alt="fligh snapptrip"
          layout="responsive"
          // width={100}
          // height={40}
        /> */}
        <img src="/images/flight.svg" alt="fligh snapptrip" />

        <div className={styles.backgroundLogo} onClick={() => {
          dispatch(increment());
        }}>
          {/* <Image
            src={snapptrip}
            alt="snapptrip logo"
            layout="fill"
          /> */}
          <img src="/images/snapptrip.svg" alt="snapptrip logo" />
        </div>
      </div>

      <div className={styles.main}>
        <div className={styles.mainBox}>
          <div className={styles.mainTitle}>
            <h2>{Texts.MAIN_ACTIONS_TITLE}</h2>
          </div>

          <div className={styles.mainActionsContainer}>
            {actions.map(function(item) {
              return (
                <Action
                  key={item.route}
                  title={item.title}
                  image={item.image}
                  route={item.route}
                  enabled={item.enabled}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Main;
