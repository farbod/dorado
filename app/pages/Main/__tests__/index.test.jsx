import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import renderer from 'react-test-renderer';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import Main from '../';

describe('Main', () => {
  const initialState = {
    main: {
      value: 0,
      status: ''
    }
  };
  const mockStore = configureStore();
  let store = {};
  let container = null;

  beforeEach(() => {
    store = mockStore(initialState);
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    store = null;
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  test('Shows Text', () => {
    const { getByText } = render(<Provider store={store}><Main /></Provider>)
    expect(getByText('خدمات اسنپ برای سفر')).not.toBeNull()
  })

  it('renders a heading', () => {
    let store = mockStore(initialState)
    render(<Provider store={store}><Main /></Provider>)
    const heading = screen.getByRole('heading', {
      name: 'خدمات اسنپ برای سفر',
    });
    expect(heading).toBeInTheDocument()
  })

  // test('match snapshot', () => {
  //   let store = mockStore(initialState)
  //   const component = renderer.create(<Provider store={store}><Main /></Provider>);
  //   let tree = component.toJSON();
  //   expect(tree).toMatchSnapshot();
  // })
});
