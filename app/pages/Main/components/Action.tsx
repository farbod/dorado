import Link from 'next/link';

import styles from './Action.module.scss';

type AppProps = {
  title   : string,
  image   : string,
  route   : string,
  enabled : boolean
};

const defaultProps = {
  title   : '',
  image   : '',
  route   : '',
  enabled : false
};

const Action = ({ title, image, route, enabled }: AppProps): JSX.Element => {
  const body = (
    <div className={`${styles.container} ${enabled ? styles.enabled : ''}`}>
      <img
        src={image}
        alt={title}
      />

      <h5>{title}</h5>
    </div>
  );

  if (enabled) {
    return (
      <Link href={route}>
        <a>
          {body}
        </a>
      </Link>
    );
  }
  return body;
};
Action.defaultProps = defaultProps;

export { Action };