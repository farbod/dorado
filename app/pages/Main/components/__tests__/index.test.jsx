import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import renderer from 'react-test-renderer';
// import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { Action } from '../Action';

describe('Main/Action', () => {
  let container = null;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  test('Shows Text', () => {
    act(() => {
      render(<Action title="test title" image="/" route="" enabled />, container);
    });
    expect(container.textContent).toBe('test title');
  })

  // test('Shows Text', () => {
  //   const { getByText } = render(<Action title="test title" image="/" route="" enabled />)
  //   expect(getByText('test title')).not.toBeNull()
  // })

  // it('renders a heading', () => {
  //   render(<Action title="test title" image="/" route="" enabled />)
  //   const heading = screen.getByRole('heading', {
  //     name: 'test title',
  //   })
  //   expect(heading).toBeInTheDocument()
  // })
});
