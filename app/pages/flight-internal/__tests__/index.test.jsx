import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import renderer from 'react-test-renderer';
import { act, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import Component from '../';

describe('Main', () => {
  const initialState = {
    global: {
      cities: [
        { id: '1', name: 'name1', nameEnglish: 'name English1', airport:'airport1' },
        { id: '2', name: 'name2', nameEnglish: 'name English2', airport:'airport2' }
      ]
    },
    flightInternal: {
      sourceId      : '1',
      destinationId : '2'
    }
  };
  const mockStore = configureStore();
  let store = {};
  let container = null;

  beforeEach(() => {
    store = mockStore(initialState);
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    store = null;
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  test('click it', () => {
    // const onChange = jest.fn();
    act(() => {
      render(<Provider store={store}><Component /></Provider>, container);
    });

    const switchButton = document.querySelector('[data-test-id=flight-internal-switch-button]');
    const source = document.querySelector('[data-test-id=flight-internal-source]');
    const destination = document.querySelector('[data-test-id=flight-internal-destination]');

    expect(source.innerHTML).toBe('name1');
    expect(destination.innerHTML).toBe('name2');

    act(() => {
      switchButton.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const actions = store.getActions();
    const expectedActionsList = [
      { type: 'flight-internal/changeSourceId', payload: '2' },
      { type: 'flight-internal/changeDestinationId', payload: '1' }
    ];
    expect(actions).toEqual(expectedActionsList);

    // expect(source.innerHTML).toBe('name2');
    // expect(destination.innerHTML).toBe('name1');

    // const { getByText } = render(<Provider store={store}><Component /></Provider>)
    // expect(getByText('خدمات اسنپ برای سفر')).not.toBeNull()
  })
});
