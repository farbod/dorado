import { useState } from 'react';
import {
  City
} from '../../../slice';

import styles from './PickCity.module.scss';

type AppProps = {
  placeholder : string,
  cities      : City[],
  setCityId   : (id: string) => void,
  close       : () => void
};

const defaultProps = {
  placeholder : '',
  cities      : [],
  setCityId   : function() {},
  close       : function() {}
};

const PickCity = ({ placeholder, cities, setCityId, close }: AppProps): JSX.Element => {
  const [ query, setQuery ] = useState('');

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div
          className={styles.closeButton}
          onClick={close}
        >
          <i className="material-icons">close</i>
        </div>
      </div>

      <div className={styles.searchBox}>
        <input
          type="text"
          placeholder={placeholder}
          value={query}
          onChange={function(e) {
            setQuery(e.target.value);
          }}
        />

        {query === '' ?        
          <div className={styles.searchButton}>
            <i className="material-icons">search</i>
          </div>
          :
          <div
            className={styles.clearButton}
            onClick={function() {
              setQuery('');
            }}
          >
            <i className="material-icons">cancel</i>
          </div>
        }
      </div>

      <div className={styles.list}>
        {cities
          .filter(function(item) {
            if (
              item.name.includes(query) ||
              item.nameEnglish.includes(query.toUpperCase()) ||
              item.airport.includes(query)
            ) {
              return true;
            }
            return false;
          })
          .map(function(item, index) {
            return (
              <div
                key={item.id}
                className={`${styles.item} ${index !== cities.length - 1 ? styles.borderBottom : ''}`}
                onClick={function() {
                  setCityId(item.id);
                  close();
                }}
              >
                <div className={styles.itemIcon}>
                  <i className="material-icons">location_on</i>
                </div>

                <div className={styles.itemMain}>
                  <div className={styles.itemName}>{item.name} {item.nameEnglish}</div>
                  <div className={styles.itemAirport}>{item.airport}</div>
                </div>
              </div>
            );
          })
        }
      </div>
    </div>
  );
};
PickCity.defaultProps = defaultProps;

export { PickCity };