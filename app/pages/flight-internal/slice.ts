import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { put, takeEvery } from 'redux-saga/effects';

import { AppThunk } from '../../store';

export interface State {
  sourceId      : string,
  destinationId : string,
  loading       : boolean
}

const INITIAL_STATE: State = {
  sourceId      : '',
  destinationId : '',
  loading       : false
};

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))

function* searchSubmit(action: { type: string, params: object }) {
  yield put(changeLoading(true));
  console.log(action.params);
  yield delay(3000);
  yield put(changeLoading(false));
}

export function* watchSearchSubmit() {
  yield takeEvery('SEARCH_SUBMIT', searchSubmit)
}

export const slice = createSlice({
  name: 'flight-internal',
  initialState: INITIAL_STATE,
  reducers: {
    changeSourceId: (state, action: PayloadAction<string>) => {
      state.sourceId = action.payload;
    },
    changeDestinationId: (state, action: PayloadAction<string>) => {
      state.destinationId = action.payload;
    },
    changeLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    }
  }
});

export const { changeSourceId, changeDestinationId, changeLoading } = slice.actions;

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.
// export const incrementIfOdd = (amount: number): AppThunk => (
//   dispatch,
//   getState
// ) => {
//   const currentValue = selectCount(getState());
//   if (currentValue % 2 === 1) {
//     dispatch(incrementByAmount(amount));
//   }
// };

export default slice.reducer;