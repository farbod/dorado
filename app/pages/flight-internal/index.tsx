import { useState, useEffect, useMemo } from 'react';
import { batch } from 'react-redux';
import { useRouter } from 'next/router';
import type { NextPage } from 'next';

import {
  Texts
} from '../../constants';
import { RootState } from '../../store';
import { useAppSelector, useAppDispatch } from '../../hooks';
import {
  getCities,
  State as GlobalState
} from '../../slice';
import {
  Modal,
  useModal
} from '../../components';
import {
  changeSourceId,
  changeDestinationId,
  State
} from './slice';
import {
  PickCity as PickCityModal
} from './modals';

import styles from './index.module.scss';

const FlightInernal: NextPage = () => {
  // make dispatch ready
  const dispatch = useAppDispatch();

  // router ready
  const router = useRouter();

  // get state
  const selectState = (state: RootState): State => {
    return {
      sourceId      : state.flightInternal.sourceId,
      destinationId : state.flightInternal.destinationId,
      loading       : state.flightInternal.loading
    }
  };
  const state = useAppSelector(selectState);

  // get global state
  const selectGlobalState = (state: RootState): GlobalState => {
    return {
      cities : state.global.cities
    }
  };
  const globalState = useAppSelector(selectGlobalState);

  // states
  const [ isOneWay, setIsOneWay ] = useState<boolean>(true);
  const { status: modalStatus, view: modalView, show: modalShow, close: modalClose } = useModal();

  // fetch cities
  useEffect(() => {
    if (globalState.cities.length === 0) {
      dispatch(getCities());
    }

    return () => {
    }
  }, [])

  // find source from cities array
  const source = useMemo(() => {
    return globalState.cities.find(function(item) {
      return item.id === state.sourceId;
    });
  }, [state.sourceId]);

  // find destination from cities array
  const destination = useMemo(() => {
    return globalState.cities.find(function(item) {
      return item.id === state.destinationId;
    });
  }, [state.destinationId]);

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div
          className={styles.backButton}
          onClick={function() {
            router.back();
          }}
        >
          <i className="material-icons">chevron_right</i>
        </div>
      </div>

      <h2>{Texts.INTERNAL_FLIGHT_TITLE}</h2>

      <div className={styles.cover}>
        <img
          src="/images/flight-home.svg"
          alt="flight home"
        />
      </div>

      <div className={styles.waySwitch}>
        <div
          className={`${styles.way} ${isOneWay ? styles.active : ''}`}
          onClick={function() {
            if (!isOneWay) {
              setIsOneWay(true);
            }
          }}
        >
          یک‌طرفه
        </div>

        <div
          className={`${styles.way} ${isOneWay ? '' : styles.active}`}
          onClick={function() {
            if (isOneWay) {
              setIsOneWay(false);
            }
          }}
        >
          دوطرفه
        </div>
      </div>

      <div className={styles.addressBox}>
        <div className={styles.addressesContainer}>
          <div
            className={styles.address}
            onClick={function() {
              modalShow(
                <PickCityModal
                  placeholder="جست‌وجوی مبدا"
                  cities={globalState.cities}
                  setCityId={function(id) {
                    dispatch(changeSourceId(id));
                  }}
                  close={modalClose}
                />
              );
            }}
          >
            <div className={styles.addressIcon}>
              <i className="material-icons">radio_button_unchecked</i>
            </div>

            <div data-test-id="flight-internal-source" className={`${styles.addressText} ${styles.borderBottom} ${state.sourceId === '' ? styles.placeholder : ''}`}>
              {state.sourceId === '' ? 'مبدا' : (source ? source.name : '')}
            </div>
          </div>

          <div
            className={styles.address}
            onClick={function() {
              modalShow(
                <PickCityModal
                  placeholder="جست‌وجوی مقصد"
                  cities={globalState.cities}
                  setCityId={function(id) {
                    dispatch(changeDestinationId(id));
                  }}
                  close={modalClose}
                />
              );
            }}
          >
            <div className={`${styles.addressIcon} ${styles.green}`}>
              <i className="material-icons">location_on</i>
            </div>

            <div data-test-id="flight-internal-destination" className={`${styles.addressText} ${state.destinationId === '' ? styles.placeholder : ''}`}>
              {state.destinationId === '' ? 'مقصد' : (destination ? destination.name : '')}
            </div>
          </div>
        </div>

        <div
          data-test-id="flight-internal-switch-button"
          className={styles.addressSwitchButton}
          onClick={function() {
            batch(() => {
              dispatch(changeSourceId(state.destinationId));
              dispatch(changeDestinationId(state.sourceId));
            });
          }}
        >
          <i className="material-icons">swap_vert</i>
        </div>
      </div>

      <div className={styles.dateBox}>
        <div className={styles.dateIcon}>
          <i className="material-icons">event</i>
        </div>

        <div className={styles.dateMain}>
          <div className={styles.dateTitle}>
            {isOneWay ? 'تاریخ حرکت' : 'تاریخ رفت و برگشت'}
          </div>

          <div className={styles.dateValue}>
            ۱ فروردین
          </div>
        </div>
      </div>

      <div className={styles.passengerBox}>
        <div className={styles.passengerIcon}>
          <i className="material-icons">person</i>
        </div>

        <div className={styles.passengerMain}>
          <div className={styles.passengerTitle}>
            تعداد مسافر
          </div>

          <div className={styles.passengerValue}>
            ۱ بزرگسال
          </div>
        </div>
      </div>

      <div
        className={styles.submit}
        onClick={function() {
          dispatch({ type: 'SEARCH_SUBMIT', params : { sourceId: state.sourceId, destinationId: state.destinationId } });
        }}
      >
        {state.loading ? 'loading' : 'جست‌وجو'}
      </div>

      <Modal
        status={modalStatus}
        view={modalView}
        close={modalClose}
      />
    </div>
  );
};

export default FlightInernal;
