import { all } from 'redux-saga/effects';

import { watchSearchSubmit } from './pages/flight-internal/slice';

export default function* rootSaga() {
  yield all([
    watchSearchSubmit()
  ])
}