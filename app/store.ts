import {
  configureStore,
  ThunkAction,
  Action
} from '@reduxjs/toolkit';
import createSagaMiddleware from "redux-saga";

import global         from './slice';
import main           from './pages/Main/slice';
import flightInternal from './pages/flight-internal/slice';

import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    global,
    main,
    flightInternal
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
