import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk } from './store';

export interface City {
  id          : string,
  name        : string,
  nameEnglish : string,
  airport     : string
};

// interface Cities {
//   [key: string]: City
// };

export interface State {
  cities: City[]
}

const INITIAL_STATE: State = {
  cities: []
};

export const getCities = createAsyncThunk(
  'global/get-cities',
  async () => {
    const response = [
      { id: '12341', name: 'تهران',  nameEnglish: 'THR', airport: 'فرودگاه بین‌المللی مهرآباد' },
      { id: '12342', name: 'مشهد',   nameEnglish: 'MHD', airport: 'فرودگاه بین‌المللی مشهد' },
      { id: '12343', name: 'کیش',    nameEnglish: 'KIH', airport: 'فرودگاه بین‌المللی کیش' },
      { id: '12344', name: 'اصفهان', nameEnglish: 'IFN', airport: 'فرودگاه اصفهان' },
      { id: '12345', name: 'شیراز',  nameEnglish: 'SYZ', airport: 'فرودگاه بین‌المللی شیراز' }
    ];
    return response;
  }
);

export const slice = createSlice({
  name: 'global',
  initialState: INITIAL_STATE,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCities.pending, (state) => {
      })
      .addCase(getCities.fulfilled, (state, action: PayloadAction<City[]>) => {
        state.cities = action.payload;
      });
  },
});

// export const {  } = slice.actions;

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.
// export const incrementIfOdd = (amount: number): AppThunk => (
//   dispatch,
//   getState
// ) => {
//   const currentValue = selectCount(getState());
//   if (currentValue % 2 === 1) {
//     dispatch(incrementByAmount(amount));
//   }
// };

export default slice.reducer;