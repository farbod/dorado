import { useState } from 'react';
import ReactDOM from 'react-dom';

import styles from './Modal.module.scss';

enum Status {
  Show,
  Hide,
  IsHiding
};

type AppProps = {
  status : Status,
  view   : JSX.Element,
  close  : () => void
};

const defaultProps = {
  status : Status.Hide,
  view   : null,
  close  : function() {}
};

const Modal = ({ status, view, close }: AppProps): JSX.Element => {
  if (status === Status.Show || status === Status.IsHiding) {
    const rootElement = document.getElementById('__next');

    if (rootElement) {
      return ReactDOM.createPortal(
        <div
          className={`
            ${styles.container}
            ${status === Status.Show ? styles.show : ''}
            ${status === Status.IsHiding ? styles.goHide : ''}
          `}
          onClick={function() {
            close();
          }}
        >
          <div
            className={`
              ${styles.box}
              ${status === Status.Show ? styles.show : ''}
              ${status === Status.IsHiding ? styles.goHide : ''}
            `}
            onClick={function(e) {
              e.stopPropagation();
            }}
          >
            {view}
          </div>
        </div>
  
        , rootElement
      );
    // end check rootElement
    }
    return null as any;
  // end check isShow
  }
  return null as any;
};
Modal.defaultProps = defaultProps;

export { Modal };


function useModal() {
  const [ status, setStatus ] = useState<Status>(Status.Hide);
  const [ view, setView ] = useState<JSX.Element>(null as any);

  const close = () => {
    setStatus(Status.IsHiding);
    setTimeout(() => {
      setStatus(Status.Hide);
    }, 300);
  }

  const show = (view_: JSX.Element) => {
    setStatus(Status.Show);
    setView(view_);
  }

  return { status, view, show, close };
}

export { useModal };
