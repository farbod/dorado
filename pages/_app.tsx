// import { useEffect } from 'react';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
// import { Workbox } from 'workbox-window';

import { store } from '../app/store';

import '../styles/globals.scss';
import '../styles/fontiran.scss';
import '../styles/materialIcons.scss';

function MyApp({ Component, pageProps }: AppProps) {
  // useEffect(() => {
  //   if ('serviceWorker' in navigator) {
  //     console.log('serviceWorker')
  //     const wb = new Workbox("sw.js", { scope: '/' });
  //     wb.register();
  //   } else {
  //     console.log('does not support service worker');
  //   }

  //   return () => {
  //   }
  // }, [])

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;